<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Index;

class AboutController extends Controller
{
    public function about()
    {
        return view('about', [
            "title" => "About",
            "about" => About::all()
        ]);
    }
}