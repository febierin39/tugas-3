<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Index;

class ContactController extends Controller
{
    public function contact()
    {
        return view('contact', [
            "title" => "Contact",
            "contact" => Contact::all()
        ]);
    }
}
